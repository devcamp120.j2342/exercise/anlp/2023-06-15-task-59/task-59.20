package com.devcamp.task_59_20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5920Application {

	public static void main(String[] args) {
		SpringApplication.run(Task5920Application.class, args);
	}

}
