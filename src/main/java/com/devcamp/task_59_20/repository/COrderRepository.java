package com.devcamp.task_59_20.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task_59_20.models.COrder;

public interface COrderRepository extends JpaRepository<COrder, Long>{
    
}
